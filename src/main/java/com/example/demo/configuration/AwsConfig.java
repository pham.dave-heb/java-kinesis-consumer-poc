package com.example.demo.configuration;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.kinesis.AmazonKinesisAsync;
import com.amazonaws.services.kinesis.AmazonKinesisAsyncClientBuilder;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Optional;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
//@ConditionalOnProperty("aws.enabled")
@Slf4j
public class AwsConfig {

  @Configuration
  @ConfigurationProperties(prefix = "aws")
  @Data
  public static class AwsConfigProperties {

    private String assumeRole = "arn:aws:iam::561718942347:role/Staff";
    private String assumeRoleProfileConsumer = "arn:aws:iam::273820703578:role/profile-comm-prefs-crt-consumer";
    private String mfaSerial;
    private String snsTopic;
  }

  private static final String AWS_MFA_TOKEN_PROMPT = "Enter MFA Token Code for %s: ";
  private static final String ASSUME_ROLE_SESSION_NAME = "PreferencesSession";
  private static final String CREDENTIALS_PATH = "./aws_credentials.json";
  private static final int CREDENTIALS_EXPIRATION_MARGIN_MINUTES = 10;

  private final ObjectMapper objectMapper;

  public AwsConfig(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Profile("local")
  @Bean
  @Primary
  public AWSCredentialsProvider awsCredentialsProviderLocal(
      AwsConfigProperties awsConfigProperties) {
    Credentials credentials =
        getCredentialsFromFile()
            .or(() -> getNewCredentialsAndWriteToFile(awsConfigProperties))
            .orElseThrow(
                () ->
                    new RuntimeException(
                        "Could not get AWS credentials. "
                            + "If using aws-login script, ensure credentials are not expired. "
                            + "If using MFA prompt, ensure AWS_MFA_SERIAL is set."));

    return new AWSStaticCredentialsProvider(
        new BasicSessionCredentials(
            credentials.getAccessKeyId(),
            credentials.getSecretAccessKey(),
            credentials.getSessionToken()));
  }

  @Profile("!local")
  @Bean
  @Primary
  public AWSCredentialsProvider awsCredentialsProvider() {
    return new DefaultAWSCredentialsProviderChain();
  }

  @Bean
  @Profile("profile-consumer")
  public AWSCredentialsProvider awsCredentialsProviderProfile(AWSCredentialsProvider awsCredentialsProvider, AwsConfigProperties awsConfigProperties) {
    AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard().withCredentials(awsCredentialsProvider).build();
    AssumeRoleRequest assumeRoleRequest =
        new AssumeRoleRequest()
            .withRoleArn(awsConfigProperties.getAssumeRoleProfileConsumer())
            .withRoleSessionName(ASSUME_ROLE_SESSION_NAME);
    Credentials credentials = sts.assumeRole(assumeRoleRequest).getCredentials();
    return new AWSStaticCredentialsProvider(
        new BasicSessionCredentials(
            credentials.getAccessKeyId(),
            credentials.getSecretAccessKey(),
            credentials.getSessionToken()));
  }

  @Bean
  public AmazonDynamoDB amazonDynamoDBAsync(AWSCredentialsProvider awsCredentialsProvider) {
    return AmazonDynamoDBAsyncClientBuilder.standard().withCredentials(awsCredentialsProvider)
        .build();
  }

  @Bean
  @Profile("profile-consumer")
  public AmazonKinesisAsync amazonKinesisAsyncProfile(
      @Qualifier("awsCredentialsProviderProfile") AWSCredentialsProvider awsCredentialsProvider) {
    return AmazonKinesisAsyncClientBuilder.standard().withCredentials(awsCredentialsProvider)
        .build();
  }

  @Bean
  @Profile("!profile-consumer")
  public AmazonKinesisAsync amazonKinesisAsync(AWSCredentialsProvider awsCredentialsProvider) {
    return AmazonKinesisAsyncClientBuilder.standard().withCredentials(awsCredentialsProvider)
        .build();
  }

//  @Bean
//  public AmazonSNS amazonSNS(AWSCredentialsProvider awsCredentialsProvider) {
//    return AmazonSNSClientBuilder.standard().withCredentials(awsCredentialsProvider).build();
//  }

//  @Bean
//  public NotificationMessagingTemplate notificationMessagingTemplate(AmazonSNS amazonSNS) {
//    return new NotificationMessagingTemplate(amazonSNS);
//  }

  @SuppressWarnings("PMD.SystemPrintln")
  private String captureTokenCodeFromConsole(String mfaSerial) {
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in, Charset.defaultCharset()));
    try {
      System.out.printf(AWS_MFA_TOKEN_PROMPT, mfaSerial);
      return reader.readLine();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Optional<Credentials> getCredentialsFromFile() {
    File file = new File(CREDENTIALS_PATH);

    try {
      if (file.exists()) {
        Credentials credentials = objectMapper.readValue(file, Credentials.class);
        Instant expirationMinusMargin =
            credentials
                .getExpiration()
                .toInstant()
                .plusSeconds(-60 * CREDENTIALS_EXPIRATION_MARGIN_MINUTES);
        if (Instant.now().isBefore(expirationMinusMargin)) {
          log.debug("Using cached assume role credentials");
          return Optional.of(credentials);
        }
      }
    } catch (IOException e) {
      log.warn("Could not get AWS credentials from file.", e);
    }

    return Optional.empty();
  }

  private Optional<Credentials> getNewCredentialsAndWriteToFile(
      AwsConfigProperties awsConfigProperties) {
    AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.defaultClient();

    Credentials credentials;
    AssumeRoleRequest assumeRoleRequest;
    try {
      // Assume role without MFA prompt, only works if aws-login script was used prior
      assumeRoleRequest =
          new AssumeRoleRequest()
              .withRoleArn(awsConfigProperties.assumeRole)
              .withRoleSessionName(ASSUME_ROLE_SESSION_NAME);
      credentials = sts.assumeRole(assumeRoleRequest).getCredentials();
      log.debug("Assumed role without MFA prompt");
    } catch (Exception e) {
      // Assume role with MFA prompt
      if (StringUtils.isEmpty(awsConfigProperties.mfaSerial)) {
        log.warn("MFA Serial is not set");
        return Optional.empty();
      }
      assumeRoleRequest =
          new AssumeRoleRequest()
              .withRoleArn(awsConfigProperties.assumeRole)
              .withRoleSessionName(ASSUME_ROLE_SESSION_NAME)
              .withSerialNumber(awsConfigProperties.mfaSerial)
              .withTokenCode(captureTokenCodeFromConsole(awsConfigProperties.mfaSerial));
      credentials = sts.assumeRole(assumeRoleRequest).getCredentials();
      log.debug("Assumed role with MFA prompt");
    }

    try {
      Files.writeString(Paths.get(CREDENTIALS_PATH), objectMapper.writeValueAsString(credentials));
    } catch (IOException e) {
      log.warn("Could not write AWS credentials to file.", e);
    }

    return Optional.of(credentials);
  }
}
