package com.example.demo;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.heb.profile.model.Profile;
import java.io.IOException;
import java.util.function.Consumer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = {"com.example.demo"})
public class DemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }

  @Bean
  @org.springframework.context.annotation.Profile("profile-consumer")
  public Consumer<byte[]> profileConsumer(ObjectMapper objectMapper) {
    return event -> {
      try {
        Profile profile = objectMapper.readValue(event, Profile.class);
        System.out.println("Received Profile: " + profile);
      } catch (IOException e) {
        e.printStackTrace();
        System.out.println("Received: " + new String(event));
      }
    };
  }

  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper()
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
        .setSerializationInclusion(Include.ALWAYS)
        .registerModule(new JavaTimeModule());
  }
}
